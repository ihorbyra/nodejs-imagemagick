const express = require('express');
const router = express.Router();

let Image = require('../app/controllers/ImageController');
let Upload = require('./../app/middleware/upload');

Image = new Image();
Upload = new Upload();

router.get('/', Image.index.bind(Image));
router.get('/image/:id', Image.one.bind(Image));
router.get('/process', Image.process.bind(Image));
router.post('/upload', Upload.uploadSingle(Image.mainDir()), Image.process.bind(Image));

module.exports = router;
