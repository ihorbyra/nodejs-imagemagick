const im = require("imagemagick");
const conf = require('./../services/config');
const Image = require('./../models/images');

class ImageController {

    mainDir() {
        return 'canvas';
    }

    index(req, res) {
        this.all()
            .then( (data) => {
                return res.render('image/index', {images: data});
            })
            .catch( (err) => {
                return res.render('image/index');
            });
    }

    process(req, res) {
        let dir = req.file.destination;
        let file = req.file.filename;
        let tiles = req.body.tiles;

        console.log(req.file);

        this.imageData(dir, file)
            .then( data => this.resize(data, dir, file) )
            .then( data => this.imageData(dir, file) )
            .then ( (data) => {
                return {
                    'dir': dir,
                    'file': file,
                    'tiles': tiles,
                    'fileData': data
                }
            })
            .then( data => this.divide(data) )
            .then( () => {
                let tilesArr = [];
                for (let i= 0; i < tiles; i++) {
                    tilesArr.push(`tiles_${i}.jpg`);
                }
                return {
                    'name': file,
                    'dir': file.split('.')[0],
                    'tiles_amount': tiles,
                    'tiles': tilesArr
                }
            })
            .then (data => this.save(data) )
            .then (data => console.log(data) )
            .catch( (err) => {
                console.log(err);
        });

        res.redirect('/');
    }

    imageData(dir, img) {
        return new Promise( (resolve, reject) => {
            let file = dir + '/' + img;
            im.identify(file, (err, result) => {
                if (err) return reject(err);
                return resolve(result);
            })
        })
    }

    resize(data, dir, img) {
        let width = conf.get('img:width');

        return new Promise( (resolve, reject) => {
            let file = dir + '/' + img;

            if (width < data.width) {
                im.resize({
                    srcPath: file,
                    dstPath: file,
                    width: width
                }, (err) => {
                    if (err) return reject(err);

                    return resolve({
                        'dir': dir,
                        'file': img
                    });
                });
            } else {
                return resolve({
                    'dir': dir,
                    'file': img
                });
            }
        });
    }

    divide(data) {
        let dir = data.dir;
        let file = dir + '/' + data.file;
        let tiles = data.tiles;
        let fileData = data.fileData;
        console.log(tiles);

        let width = fileData.width;
        let height = fileData.height;

        // Розумію що ця частина коду негативно вплине на думку про професійність, але робив швидко,
        // тому надіюсь, що в полі будуть вказувати значення: 2,3 або 4)
        if (tiles >= 2 && tiles <= 3) {
            width = width / tiles;
        } else if (tiles == 4) {
            width = width / 2;
            height = height / 2;
        }

        let args = [
            file,
            '-crop',
            `${width}x${height}`,
            '+repage',
            '+adjoin',
            `${dir}/tiles_%d.jpg`
        ];

        return new Promise( (resolve, reject) => {
            im.convert(args, function(err) {
                if(err) return reject(err);
                return resolve({
                    'dir': dir,
                    'file': file,
                    'tiles': ''
                });
            });
        });
    }

    save(data)
    {
        let image = new Image(data);

        return new Promise( (resolve, reject) => {
            image.save( (err) => {
                if (err) {
                    return reject({
                        success: false,
                        message: err
                    });
                } else {
                    return resolve({
                        success: true,
                        message: 'Post has been created!'
                    });
                }
            });
        });
    }

    all() {
        return new Promise( (resolve, reject) => {
            Image.find({}, (err, result) => {
                if (err) {
                    // console.log(err);
                    return reject({
                        err: err
                    });
                }
                // console.log(result);
                return resolve(result);
            });
        });
    }

    one(req, res) {
        // console.log(req.params.id);
        Image.find({_id: req.params.id}, (err, result) => {
            if (err) {
                // console.log(err);
                return res.send({
                    err: err
                });
            }
            // console.log(result);
            return res.render('image/item', {image: result[0]});
        });
    }

}

module.exports = ImageController;