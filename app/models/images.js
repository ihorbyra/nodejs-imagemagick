const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const ImagesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    dir: {
        type: String,
        required: true,
    },
    tiles_amount: {
        type: Number,
        required: true,
    },
    tiles: [{
        type: String
    }],
    created_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Image', ImagesSchema);