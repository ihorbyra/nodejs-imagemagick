const multer = require('multer');
const fs = require('fs');

const conf = require('./../services/config');
let Filesystem = require('./../services/filesystem');

Filesystem = new Filesystem();

class Upload
{
    constructor()
    {
        let dir = conf.get('app:upload_dir');
        Filesystem.dirExists(dir);
    }

    uploadSingle(location)
    {
        let dir = conf.get('app:upload_dir') + '/' + location;
        let datetimestamp = Date.now();

        Filesystem.dirExists(dir);

        let storage = multer.diskStorage({
            destination: function (req, file, callback) {

                let canvasDir = file.originalname.split('.')[0];
                let newDir = '';
                newDir = dir + '/' + canvasDir + '-' + datetimestamp;

                // console.log(newDir);
                if (fs.existsSync(newDir)) {
                    let datetimestamp = Date.now();
                    newDir = dir + '/' + canvasDir + '-' + datetimestamp;
                        Filesystem.makeDir(newDir);
                } else Filesystem.makeDir(newDir);

                callback(null, newDir);
            },

            filename: function (req, file, callback) {
                callback(null, file.originalname.split('.')[0] + '-' + datetimestamp + '.'
                    + file.originalname.split('.')[file.originalname.split('.').length -1]);
            }
        });

        return multer({storage: storage}).single('file');
    }
}

module.exports = Upload;