const nconf = require('nconf');
const path = require('path');
const fs = require('fs');

let config = './config';

nconf.argv().env();

fs.readdirSync(config).forEach(function(file) {
    if (file.slice(-4) === 'json') {
        let key = file.slice(0, -5);
        nconf.file(key, path.join(config, file));
    }
});

module.exports = nconf;