const mongoose = require('mongoose');
const config = require('./config');

mongoose.connect(config.get('db:mongodb:mongoose:uri'), config.get('db:mongodb:mongoose:options'));

module.exports = mongoose;