const fs = require('fs');

const conf = require('./config');

class Filesystem
{
    dirExists(path)
    {
        if (!fs.existsSync(path)) {

            this.makeDir(path);
        }
    }

    makeDir(path)
    {
        fs.mkdir(path, function (err) {
            if (err) {
                console.log('Failed to create directory', err);
            } else {
                console.log('Directory was succesfully created');
            }
        });
    }
}

module.exports = Filesystem;